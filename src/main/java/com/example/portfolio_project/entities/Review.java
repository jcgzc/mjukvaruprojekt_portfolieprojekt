package com.example.portfolio_project.entities;

import javax.persistence.*;

@Entity
@Table(name ="reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rId;

    @Column(length = 45)
    private String date;

    @Column(length = 45)
    private String time;

    @Column(length = 45)
    private String selection;

    @Column(length = 45)
    private String opinions;

    @Column(length = 45)
    private boolean approval;

    @Column(length = 45)
    private boolean disapproval;

    @ManyToOne
    @JoinColumn (name ="accid")
    private Accounts accounts;

    @OneToOne
    @JoinColumn (name="bookingId")
    private Booking booking;


    public Integer getrId() {
        return rId;
    }

    public void setrId(Integer rId) {
        this.rId = rId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getOpinions() {
        return opinions;
    }

    public void setOpinions(String opinions) {
        this.opinions = opinions;
    }

    public boolean isApproval() {
        return approval;
    }

    public void setApproval(boolean approval) {
        this.approval = approval;
    }

    public boolean isDisapproval() {
        return disapproval;
    }

    public void setDisapproval(boolean disapproval) {
        this.disapproval = disapproval;
    }



    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
}
