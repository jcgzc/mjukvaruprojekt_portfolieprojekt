package com.example.portfolio_project.repository;


import com.example.portfolio_project.entities.Cleaners;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CleanersRepository extends JpaRepository<Cleaners, Integer> {

    Cleaners findByEmailAndPassword(String email, String password);
}
