package com.example.portfolio_project.service;

import com.example.portfolio_project.entities.Booking;
import com.example.portfolio_project.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookingService {

    @Autowired private BookingRepository repo;

    public List<Booking> listAllBooking() { return (List<Booking>) repo.findAll();}


    public void addBooking(Booking booking){ repo.save(booking);}
    public void editBooking(Booking booking) { repo.save(booking);}

    public Booking get(Integer id) throws UserNotFoundException {

        Optional<Booking> result = repo.findById(id);

        if(result.isPresent()){
            return result.get();

        }
        throw
                new UserNotFoundException("Could not find Boooking with id" + id);
    }



    public void deleteBooking(Integer id){
        repo.deleteById(id);
    }



}
