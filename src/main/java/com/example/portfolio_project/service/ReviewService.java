package com.example.portfolio_project.service;


import com.example.portfolio_project.entities.Booking;
import com.example.portfolio_project.entities.Review;
import com.example.portfolio_project.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ReviewService {

    @Autowired
    private ReviewRepository repo;

    public void addReview(Review review){ repo.save(review);}


    public Review approveReview(Integer bookingId) {
        Review review = repo.getById(bookingId);
        review.setApproval(true);
        return repo.save(review);
    }

}
