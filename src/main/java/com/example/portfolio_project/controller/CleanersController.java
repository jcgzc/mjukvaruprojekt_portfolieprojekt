package com.example.portfolio_project.controller;

import com.example.portfolio_project.entities.Accounts;
import com.example.portfolio_project.entities.Cleaners;
import com.example.portfolio_project.service.CleanersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

@Controller
@SessionAttributes("cleaners")
public class CleanersController {

    @Autowired
    private CleanersService cleanersService;

    @GetMapping("/cleaners/login_page")
    public String logIn( Model model){


        model.addAttribute("cleaners", new Cleaners());
        return "login_personal";
    }


    @PostMapping("/cleaners/login")
    public String logInVal(Accounts accounts, Model model,  RedirectAttributes redirectAttributes){
        Cleaners auth = cleanersService.validate(accounts.getEmail(),accounts.getPassword());

        if(Objects.nonNull(auth)){
            model.addAttribute("cleaners", auth);

            //redirectAttributes.addAttribute("auth", auth);
            return "redirect:/cleaners/my_page";
        }
        else {
            return "redirect:/accounts/login";
        }
    }

    @GetMapping("/cleaners/my_page")
    private String viewCleanersPage(){
        return "cleaners_page";
    }

    @GetMapping("/cleaners/assigned_bookings")
    private String viewBookingtoCleaners(){
        return "assigned_page";
    }
}
